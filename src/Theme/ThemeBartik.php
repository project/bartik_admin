<?php

namespace Drupal\bartik_admin\Theme;

use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Routing\AdminContext;

/**
 *
 */
class ThemeBartik implements ThemeNegotiatorInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * The route admin context to determine whether a route is an admin one.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * Creates a new AdminNegotiator instance.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The current user.
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   *   The route admin context to determine whether the route is an admin one.
   */
  public function __construct(AccountInterface $user, AdminContext $admin_context) {
    $this->user = $user;
    $this->adminContext = $admin_context;
  }

  /**
   * If the applies method returns true, Drupal calls the determineActiveTheme method.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *
   * @return bool
   */
  public function applies(RouteMatchInterface $route_match) {
    return ($this->user->hasPermission('administer') || $this->adminContext->isAdminRoute($route_match->getRouteObject())) ? TRUE : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    return $this->adminThemeRoute($route_match) ?: NULL;
  }

  /**
   * Function that does return the actual theme name.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *
   * @return bool|string
   */
  private function adminThemeRoute(RouteMatchInterface $route_match) {

    $themes_list = [];
    $bartik_admin_settings = \Drupal::config('bartik_admin.settings');

    // Check if only the superadmin user.
    if ($this->user->id() == 1) {
      $themes = \Drupal::service('theme_handler')->listInfo();
      foreach ($themes as $key => $value) {
        $themes_list[$key] = $key;
      }

      $path_is_admin = $this->adminContext->isAdminRoute($route_match->getRouteObject());
      $current_path = \Drupal::service('path.current')->getPath();
      $admin_access = $this->user->hasPermission('administer');
      $active = $bartik_admin_settings->get('bartik_admin_active');

      if ($active) {
        // Check if administer access & admin pages.
        if (($admin_access && $path_is_admin) || ($route_match->getRouteName() == "bartik_admin.admin_settings_form")) {
          $theme = $bartik_admin_settings->get('bartik_admin_theme');
        }
        else {
          $theme = $bartik_admin_settings->get('bartik_admin_page');
        }

        if (array_key_exists($theme, $themes_list)) {
          return $theme;
        }
      }
    }
  }

}
