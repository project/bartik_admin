<?php

namespace Drupal\bartik_admin\Form;

use Drupal\Core\Link;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\user\UserInterface;
use Drupal\Core\Url;

/**
 * Defines a form that configures settings.
 */
class BartikAdmin extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bartikadmin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'bartik_admin.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL, UserInterface $user = NULL) {

    $form_state->disableCache();

    if (isset($user)) {
      $form_state->set('user', $user);
    }

    $bartikadmin_config = $this->config('bartik_admin.settings');

    $themes = \Drupal::service('theme_handler')->listInfo();
    foreach ($themes as $key => $value) {
      $themes_list[$key] = $this->t($key);
    }

    $form['bartik_admin_active'] = [
      '#type'          => 'checkbox',
      '#title'         => t('Activate the admin theme.'),
      '#default_value' => $bartikadmin_config->get('bartik_admin_active') ? $bartikadmin_config->get('bartik_admin_active') : 0,
      '#prefix'        => "<p><i>Note: It's applicable only on superadmin user pages( uid as '1').</i></p>",
    ];

    $form['bartik_admin_page'] = [
      '#type'          => 'select',
      '#title'         => 'Page theme',
      '#options'       => $themes_list,
      '#default_value' => $bartikadmin_config->get('bartik_admin_page') ? $bartikadmin_config->get('bartik_admin_page') : 'bartik',
      '#description'   => 'It will be override the all pages theme. Only enabled themes are shown.',
    ];

    $form['bartik_admin_theme'] = [
      '#type'          => 'select',
      '#title'         => 'Administration theme',
      '#options'       => $themes_list,
      '#default_value' => $bartikadmin_config->get('bartik_admin_theme') ? $bartikadmin_config->get('bartik_admin_theme') : 'seven',
      '#description'   => 'Use the administration theme when editing or creating content.',
      '#suffix'        => '<i>Set and configure the theme for your website ' . Link::fromTextAndUrl(t('Click here'), Url::fromRoute('system.themes_page'))->toString() . '</i>',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validation here.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->config('bartik_admin.settings')
      ->set('bartik_admin_active', $values['bartik_admin_active'])
      ->set('bartik_admin_page', $values['bartik_admin_page'])
      ->set('bartik_admin_theme', $values['bartik_admin_theme'])
      ->save();
  }

  /**
   * @param \Drupal\user\UserInterface $user
   *   The account to use in the form.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   An access result object.
   */
  public function checkAccess(UserInterface $user) {
    $account = $this->currentUser();

    return AccessResult::allowedIfHasPermission($account, 'administer')
      ->andIf(AccessResult::allowedIf($user->id() == 1));
  }

}
